{-
   File      :  Galaga.hs
   Represents the main starting point for the game
-}
module Main where

import World
import Player
import GameConstants
import EventHandler
import GameState
import ScoreFile
import qualified Menus
import Selection
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game

window :: Display
window = InWindow "Galaga" screenSize (10,10)

initState :: GameState
initState = MainMenu StartGame

main :: IO ()
main = playIO window black fps initState Main.render eventHandler gameLoop

render :: GameState -> IO Picture
render (MainMenu sel) =  Menus.mainMenu sel
render HighScores     =  Menus.highScores
render (GameOver _)   =  return Menus.gameOver
render (Game world)   =  World.render world

{- gameLoop
 -
 - fairly straightworward. checks only to see that game hasn't finished either
 - through the loss condition (player has no lives) or the win condition
 - (player has reached level 5). in either case, the game is over, and the 
 - player's score is passed to the GameOver state
 -}
gameLoop :: Float -> GameState -> IO GameState
gameLoop deltaTime (Game world) = 
    let p = player world
        s = score p
        l = level p
        livesLeft = lives p
    in if (livesLeft == 0) || (l == 5)
        then return $ GameOver $ Just s
        else return $ Game (World.update deltaTime world)

{- if `GameOver` is passed a score, write it to the file, then return a 
 - `GameOver` state with no score attached. this will be caught by the 
 - catchall pattern and loop until the user decides to leave it
 -}
gameLoop _ (GameOver (Just score)) = do 
    writeScore score
    return $ GameOver Nothing    

gameLoop _ state = do
    return state
