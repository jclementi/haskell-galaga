{- gamestate module
 -
 - refactored the GameState out into its own module so that 
 - multiple files could know about it at once without
 - having to redefine it. both the eventhandler and Galaga.hs
 - needed it, for example.
 -
 - expanded the `MainMenu` to take a `Selection` parameter 
 - indicating the currently selected item.
 -
 - expanded the `GameOver` to maybe take a highscare and 
 - write it to a file. this is a `maybe` int because only
 - the first 'iteration' of the GameOver menu needs to try
 - writing the score. the player can hang out on the screen 
 - at 60 fps, but we only want to write the score once. 
 - subsequent iterations of `GameOver` are all passed `Nothing`s
 -
 -}
module GameState (GameState(..)) where

import World
import Selection

data GameState = MainMenu Selection
               | Game World
               | HighScores
               | GameOver (Maybe Int)
