{- EventHandler.hs
 -
 - contains all of the code that handles events for the various game states.
 - each state handles events differently, so it matches on gamestate to provide
 - the correct response.
 -}
module EventHandler ( eventHandler ) where

import World
import Ship
import Bullet
import GameState
import Selection
import GameConstants
import Graphics.Gloss.Interface.IO.Game

eventHandler :: Event -> GameState -> IO GameState

-- events for main menu
eventHandler (EventKey (Char key) Up _ _) (MainMenu sel) = case key of
    _   -> return $ MainMenu sel

eventHandler (EventKey (SpecialKey key) Down _ _) (MainMenu sel) = case key of
    KeyUp   -> return $ MainMenu (Selection.prev sel)
    KeyDown -> return $ MainMenu (Selection.next sel)
    KeyEnter-> return $ case (sel) of StartGame -> Game mkWorld; ShowScore -> HighScores
    _       -> return $ MainMenu sel

-- events for high scores
eventHandler (EventKey (Char key) Up _ _) HighScores = case key of
    _   -> return HighScores

eventHandler (EventKey (SpecialKey key) Down _ _) HighScores = case key of
    KeyEnter -> return $ MainMenu StartGame
    _   -> return HighScores

-- events for game over
eventHandler (EventKey (Char key) Up _ _) (GameOver s) = case key of
    _   -> return $ GameOver s

eventHandler (EventKey (SpecialKey key) Down _ _) (GameOver s) = case key of
    KeyEnter -> return $ MainMenu StartGame
    _   -> return $ GameOver s

-- events for in-game
{- the only actions here control the ship and activate the shield. see the
 - Ship module for more information abot the movement implementation. 
 -}
eventHandler (EventKey (Char key) Up _ _) (Game world) = case key of
    's' -> return $ Game $ activateShield world
    _   -> return $ Game world

eventHandler (EventKey (SpecialKey key) Up _ _) (Game world) = case key of
    KeyEsc      -> return $ MainMenu StartGame
    KeyUp       -> return $ Game (world { ship = changeVacc (-naturalAcceleration) (ship world)})
    KeyDown     -> return $ Game (world { ship = changeVacc naturalAcceleration (ship world)})
    KeyLeft     -> return $ Game (world { ship = changeHacc naturalAcceleration (ship world)})
    KeyRight    -> return $ Game (world { ship = changeHacc (-naturalAcceleration) (ship world)})
    KeySpace    -> return $ Game (world { bullets = mkBullet Friend (Ship.loc (ship world)):(bullets world)})
    _           -> return $ Game world

eventHandler (EventKey (SpecialKey key) Down _ _) (Game world) = case key of
    KeyUp       -> return $ Game (world { ship = changeVacc naturalAcceleration (ship world)})
    KeyDown     -> return $ Game (world { ship = changeVacc (-naturalAcceleration) (ship world)})
    KeyLeft     -> return $ Game (world { ship = changeHacc (-naturalAcceleration) (ship world)})
    KeyRight    -> return $ Game (world { ship = changeHacc naturalAcceleration (ship world)})
    _           -> return $ Game world

eventHandler _ state = return state

