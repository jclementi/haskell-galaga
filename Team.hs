{- team module
 -
 - this enumeration was used to mark bullets as belonging to one team or
 - the other. collision detection makes use of it to ensure enemies don't
 - kill each other.
 -
 - `Friend` corresponds to belonging to the player character. `Foe` belongs
 - to an entity hostile to the player.
 -}
module Team ( Team(..) ) where

data Team = Friend | Foe
    deriving (Show, Eq)
