{- menus module
 -
 - contains functions for rendering the various menu screens
 -}
module Menus
( mainMenu
, gameOver
, highScores
) where

import Graphics.Gloss
import Selection
import ScoreFile

{- the MainMenu is more interesting than GameOver because its the only one with
 - more than one option to select. the current selection is indicated by
 - `Selection` type passed in. its indicated to the player by the position of
 - the red `>` character next to the item's text.
 -}
mainMenu :: Selection -> IO Picture
mainMenu sel = do
    logo <- loadBMP "assets/models/galaga.bmp"
    let cursorOffset = case (sel) of
                            StartGame -> (-200)
                            ShowScore -> (-230)
    let pics = 
            [ translate (0) 0 . scale 0.6 0.6 $ logo
            , translate (-80) (-100) . scale 0.2 0.2 . color white . text $ "(kind of)"
            , translate (-350) (-200) . scale 0.2 0.2 . color white . text $ "start game"
            , translate (-350) (-230) . scale 0.2 0.2 . color white . text $ "high scores"
            , translate (-370) cursorOffset . scale 0.2 0.2 . color red . text $ ">"
            ]
    return $ Pictures pics

{- static, rendered the same way every time, nothing fancy -}
gameOver :: Picture
gameOver = 
    let pics = 
            [ translate (-200) 0 . scale 0.5 0.5 . color white . text $ "Game Over"
            , translate (-350) (-200) . scale 0.2 0.2 . color white . text $ "main menu"
            , translate (-370) (-200)  . scale 0.2 0.2 . color red . text $ ">"
            ]
    in Pictures pics

{- rending high scores
 -
 - proved to be a little tricky because each new score needed a different offset
 - from the top of the screen. I chose to accomplish this by 'tupling' each
 - score with an integer, then using the integer to calculate the appropriate
 - offset for that score.
 -}
highScores :: IO Picture
highScores = do
    scores <- readScoreList 
    let drawnScores = map drawScore (scoreTup scores)
    let pics =
            [ translate (-200) 200 . scale 0.5 0.5 . color white . text $ "High Scores"
            , translate (-350) (-200) . scale 0.2 0.2 . color white . text $ "back"
            , translate (-370) (-200)  . scale 0.2 0.2 . color red . text $ ">"
            , Pictures drawnScores
            ]
    return $ Pictures pics

{- these are just some utility functions to help draw the highscore -}
scoreTup :: [Int] -> [(Int, Int)]
scoreTup [] = []
scoreTup ss = 
    let first = head ss
    in foldl scoreTup_h [(0, first)] (drop 1 ss)

scoreTup_h :: [(Int, Int)] -> Int -> [(Int, Int)]
scoreTup_h acc@((c, _):_) score = (c+1, score):acc

drawScore :: (Int, Int) -> Picture
drawScore (offset, score) =
    let flOff = fromIntegral offset :: Float
    in translate (-100) (160 - flOff*30) . scale 0.2 0.2 . color white . text $ show score
    
