{- sprite typeclass
 -
 - a number of functions in the game make use of the sprite typeclass and the
 - functions it provides. the primary one of these is collision detection.
 -
 - the `kill` function will change the `isAlive` bool on the sprite to `False`,
 - marking it for removal in the next collision detection update.
 -}
module Sprite
( Sprite
, render
, location
, size
, kill
) where  

import Graphics.Gloss

class Sprite a where 
    render :: a -> IO Picture 
    location :: a -> Point
    size :: a -> (Int, Int) 
    kill :: a -> a
