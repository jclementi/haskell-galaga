{- selection module
 -
 - datatype used in the menus to indicate what is currently selected.
 - used by a few of the menu screens, Galaga, and event handler to 
 - properly initialize new menu screens.
 -}
module Selection 
( Selection(..)
, next
, prev
) where

data Selection = StartGame
               | ShowScore

instance Show Selection where
    show StartGame = "start game"
    show ShowScore = "high scores"

{- wanted to use `Enum`s `succ` and `pred` functions for these
 - movements, but haskell throws a runtime error when you 
 - try to take `pred` of the `0` enum, so I rolled it myself
 -}
next :: Selection -> Selection
next StartGame = ShowScore
next ShowScore = ShowScore

prev :: Selection -> Selection
prev StartGame = StartGame
prev ShowScore = StartGame
