module Ship 
( Ship(..)
, update
, mkShip
, render
, kill
, changeVacc
, changeHacc
) where 

import Graphics.Gloss
import GameConstants 
import Sprite

data Ship = Ship {
    loc :: Point, 
    isAlive :: Bool,
    isVulnerable :: Bool,
    dim :: Size,
    vSpeed :: Float,
    hSpeed :: Float,
    vAccel :: Float,
    hAccel :: Float
}

instance Sprite Ship where
    render = shipRender
    location = loc
    size = shipSize
    kill sh = sh { isAlive = False }

shipSize :: Ship -> (Int, Int)
shipSize _ = (35, 39)

{- rending ship
 -
 - nothing special here, just translating the ship's image to its location 
 - after scaling it to a reasonable size
 -}
shipRender :: Ship -> IO Picture 
shipRender ship = do
    sp <- shipPic ship
    let ship_picture = translate (fst $ loc ship) (snd $ loc ship) 
                       . scale scaleFac scaleFac $ sp
    return $ ship_picture

{- if the ship is currently invulnerable, it's represented by a blue-colored
 - ship 
 -}
shipPic :: Ship -> IO Picture  
shipPic ship = 
    if isVulnerable ship
        then loadBMP "assets/models/ship_20.bmp"
        else loadBMP "assets/models/blue_ship_20.bmp"

{- updating the location
 -
 - not much more to update. the ship's location is controlled by its speed and
 - acceleration. 'moving' the ship really only changes the acceleration. this
 - makes the movement feel very natural and smooth
 -
 - the natural acceleration and drag constants are listed in `GameConstants`
 -}
update :: Float -> Ship -> Ship
update dtime ship@(Ship { loc = (x, y) }) = 
    let newXspeed = dragSpeed((hSpeed ship) + (hAccel ship))
        newYspeed = dragSpeed((vSpeed ship) + (vAccel ship))
        newX = x + (dtime * newXspeed)
        newY = y + (dtime * newYspeed)
        nextX = if (round newX > (screenWidth `div` 2)) || round newX < (-screenWidth `div` 2)
                    then x else newX
        nextY = if (round newY > (screenHeight `div` 2)) || round newY < (-screenHeight `div` 2)
                    then y else newY
        newLoc = (nextX, nextY) 
    in ship {
        vSpeed = newYspeed,
        hSpeed = newXspeed,
        loc = newLoc }

{- used by the update function to determine the natural slowdown of the 
 - ship in space. the multiplication ensures we eventually reach a terminal
 - velocity
 -}
dragSpeed :: Float -> Float
dragSpeed speed 
    | abs speed < 1 = 0.0
    | otherwise = speed * naturalDrag

{- creating a new ship
 -
 - new ships always start in the middle-bottom of the screen, and they're
 - always given 3 seconds of invulnerability to start, which prevents the
 - 'instadeaths' you'd get when a bullet happens to pass through the spawn
 - location.
 -
 - it has to take an acceleration vector to prevent a pretty interesting bug.
 - movement changes acceleration. this is implemented by having a keydown event
 - add a certain value to the acceleration, and a keyup event subtracting that
 - same amount of acceleration. players would normally be holding down an arrow
 - key when a ship died. if the new ship had a (0.0) acceleration to start,
 - when the player released the arrowkey, the ship would have a (0,-10) 
 - acceleration when all keys were lifted, resulting in a naturally drifting
 - ship that was now impossible to move in a certain direction. to solve this
 - problem, we just pass in the acceleration vector from the dead ship, so 
 - any changes to the acceleration vector the player makes remain sensical.
 -}
mkShip :: (Float, Float) -> Ship 
mkShip (ax, ay) = Ship {
    loc = (0.0,-200.0),
    Ship.dim = (30,30),
    isAlive = True,
    isVulnerable = False,
    vSpeed = 0,
    hSpeed = 0,
    vAccel = ay,
    hAccel = ax }

changeVacc :: Float -> Ship -> Ship
changeVacc acc ship = ship { vAccel = (vAccel ship) + acc }

changeHacc :: Float -> Ship -> Ship
changeHacc acc ship = ship { hAccel = (hAccel ship) + acc }

