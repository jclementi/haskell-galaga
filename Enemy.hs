{- enemy module
 -
 - functions and definitions for the enemy datatype
 -}
module Enemy
( Enemy(..)
, update
, mkEnemy
, render
, kill
, startLevel
) where

import Graphics.Gloss
import GameConstants
import Sprite
import Movement

data Enemy = Enemy {
    loc :: Point,
    isAlive :: Bool,
    value :: Int,
    movement :: Movement,
    hSpeed :: Float,
    vSpeed :: Float,
    hAccel :: Float,
    vAccel :: Float
} deriving (Show, Eq)  

instance Sprite Enemy where
    render = enemyRender
    location = loc
    size = enemSize
    kill en = en { isAlive = False }

enemSize :: Enemy -> (Int, Int)
enemSize _ = (27, 21)

{- updating an enemy
 -
 - primarily moves it. the acceleration vector isn't controlled by the arrow
 - keys (like the player's ship's). instead, acceleration is controlled by the 
 - `Movement` currently associated with it.
 -
 - if the `Movement` is empty, it grabs the standard enemy side-to-side 
 - movement.
 -
 - the movement itself, calculating the new position, is identical to the Ship.
 -}
update :: Float -> Enemy -> Enemy
update dtime enemy = 
    let (x, y) = loc enemy
        thisMov@(mov, time) = head (movement enemy)
        curMov = (mov, time - dtime)
        menemy = if (snd curMov) < 0 
                     then enemy { movement = drop 1 (movement enemy) }
                     else enemy { movement = curMov : (drop 1 (movement enemy)) }
        movEnemy = if (movement menemy) == [] then menemy {movement = enemyMovement} else menemy
        accEnemy = setAccel movEnemy (fst $ head (movement movEnemy))
        newXspeed = dragSpeed((hSpeed accEnemy) + (hAccel accEnemy))
        newYspeed = dragSpeed((vSpeed accEnemy) + (vAccel accEnemy))
    in accEnemy {
        vSpeed = newYspeed,
        hSpeed = newXspeed,
        loc = (x + (dtime * newXspeed), y + (dtime * newYspeed)) }
        
dragSpeed :: Float -> Float
dragSpeed speed 
    | abs speed < 1 = 0.0
    | otherwise = speed * naturalDrag

setAccel :: Enemy -> AccelVec -> Enemy
setAccel en av@(ax, ay) = 
    en {
        hAccel = ax,
        vAccel = ay }
    
mkEnemy :: Point -> Enemy
mkEnemy loc = Enemy { 
    loc = loc,
    isAlive = True,
    value = 50,
    hSpeed = 0.0,
    vSpeed = 0.0,
    hAccel = 0.0,
    vAccel = 0.0,
    movement = enemyMovement}

enemyRender :: Enemy -> IO Picture
enemyRender enemy = do
    pic <- loadBMP "assets/models/blue_fighter_20.bmp"
    return $ translate (fst (loc enemy)) (snd (loc enemy)) .
                scale GameConstants.scaleFac GameConstants.scaleFac $ pic


{- functions to set the enemy formation
 -
 - each level has a slightly different enemy formation. the `startLevel`
 - function chooses the correct one based on the level integer passed to it.
 -}
startLevel :: Int -> [Enemy]
startLevel 1 = level1
startLevel 2 = level2
startLevel 3 = level3
startLevel 4 = level4
startLevel _ = level1

level1 :: [Enemy]
level1 = 
    let 
        w = fromIntegral GameConstants.screenWidth :: Float
        h = fromIntegral GameConstants.screenHeight :: Float
    in
        [ mkEnemy (x, y) | x <- [-(w/2)+150.0,-(w/2)+200.0..(w/2)-200.0], y <- [(h/2)-100.0, (h/2)-200.0..100]]

level2 :: [Enemy]
level2 = 
    let 
        w = fromIntegral GameConstants.screenWidth :: Float
        h = fromIntegral GameConstants.screenHeight :: Float
    in
        [ mkEnemy (x, y) | x <- [-(w/2)+150.0,-(w/2)+200.0..(w/2)-200.0], y <- [(h/2)-100.0, (h/2)-200.0..100]]

level3 :: [Enemy]
level3 = 
    let 
        w = fromIntegral GameConstants.screenWidth :: Float
        h = fromIntegral GameConstants.screenHeight :: Float
    in
        [ mkEnemy (x, y) | x <- [-(w/2)+150.0,-(w/2)+180.0..(w/2)-200.0], y <- [(h/2)-100.0, (h/2)-200.0..100]]

level4 :: [Enemy]
level4 = 
    let 
        w = fromIntegral GameConstants.screenWidth :: Float
        h = fromIntegral GameConstants.screenHeight :: Float
    in
        [ mkEnemy (x, y) | x <- [-(w/2)+150.0,-(w/2)+180.0..(w/2)-150.0], y <- [(h/2)-50.0, (h/2)-100.0..(h/2)-200]]
