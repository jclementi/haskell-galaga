galaga (kind of)
================

`galaga (kind of)` is a simple implementation of the classic arcade game "Galaga" written in Haskell using the Gloss graphics library.

# how to play

### controls

* arrowkeys: move ship
* spacebar: fire weapon
* `s` key: activate shield

# code overview

The code has been broken down into modules. Each module is commented in detail throughout, so I'll give only a high-level overview of everything here.

* **Galaga.hs** - Contains `main` and the game loop.
* **EventHandler.hs** - Handles all keyboard events coming from the player.
* **World.hs** - Keeps track of all the in-game entities needed for the active game.
* **Player.hs** - Maintains player statistics such a score, lives left, and powerups.
* **Menus.hs, Selection.hs** - Contains instructions for how to render the various menu screens.
* **Enemy.hs, Ship.hs, Bullet.hs, Sprite.hs, Team.hs** - entities eligible for collision, including the enemies, the player's ship, and the bullets that each of them can shoot. These are all instances of the `Sprite` typeclass.
* **Explosion.hs, Debris.hs** - explosion animations generated when an enemy or ship dies, respectively.
* **Movement.hs** - Defines movement patterns for use by enemies and other movable things.
* **GameState.hs** - The various GameStates factored out for convenience.
* **ScoreFile.hs** - Reads and writes the scores to the score file on the disk.
* **GameConstants.hs** - Contains values that control various aspects of the game.

### extra feature

I implementad a shield control. Pressing 's' activates a 5-second shield that grants invulnerability. The player only has two of these every game, so they must be rationed.

I also used this same invulnerability at the beginning of each life to make the game more playable. A player cannot respawn and instantly die thanks to the invulnerability.
