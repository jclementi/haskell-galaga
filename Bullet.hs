{- bullet module
 -
 - functions and definitions for both enemy and ship bullets
 -}
module Bullet
( Bullet(..)
, update
, mkBullet
, render
, Team(..)
) where

import Graphics.Gloss
import GameConstants
import Sprite
import Team

{- the `Team` here indicates what the bullet should be able to kill -}
data Bullet = Bullet {
    loc :: Point,
    speed :: Float,
    team :: Team,
    isAlive :: Bool
}

{- used for debugging purposes -}
instance Show Bullet where
    show = show . loc

{- bullets are 'killed' when they collide with something. they do not continue
 - into the vast expanse, juggernauting their way through the universe
 -}
instance Sprite Bullet where
    render = bulRender
    location = loc
    size = bulSize
    kill bullet = bullet { isAlive = False }

bulSize :: Bullet -> (Int, Int)
bulSize _ = (8, 13)

bulRender :: Bullet -> IO Picture
bulRender bullet = do
    bp <- loadPic bullet
    return $ translate (fst (loc bullet)) (snd (loc bullet)) .
                scale 1.0 1.0 $ bp

{- need to switch what picture is loaded based on the team the bullet belongs
 - to.
 -}
loadPic :: Bullet -> IO Picture
loadPic bullet = case (team bullet) of
    Friend  -> loadBMP "assets/models/ship_bullet_20.bmp"
    Foe     -> loadBMP "assets/models/enemy_bullet_20.bmp"

{- upding the bullet
 -
 - only the location of the bullet needs to be modified. bullets are not
 - affected by drag like the ship and enemies are. they only care about their
 - single speed
 -}
update :: Float -> Bullet -> Bullet
update dtime bullet@(Bullet { loc = (x, y) }) = bullet {
    loc = (x, y + ((speed bullet) * dtime))}

{- making the bullet
 -
 - depending on the team the bullet belongs to, the bullet will either travel
 - up the screen or down it. otherwise, the bullet is the same for both teams
 -}
mkBullet :: Team -> Point -> Bullet
mkBullet team loc = Bullet {
    team = team,
    loc = loc,
    isAlive = True,
    speed = case team of
                Friend  -> GameConstants.bulletSpeed
                Foe     -> (-GameConstants.bulletSpeed)
}
