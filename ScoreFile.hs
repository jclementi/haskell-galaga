{- ScoreFile module
 -
 - contains the functions necessary to read and write the score-saving
 - file from and to the disk.
 -}
module ScoreFile
( writeScore
, readScoreList
) where

import System.IO
import System.Directory

{- writing the score
 -
 - this takes only a single score to write. it gathers the list of current
 - high scores from the saved file, creates an integer list from them,
 - finds the proper spot in the list to insert it using `insertScore`,
 - then writes it back out to a tempFile and renames the tempfile to the
 - actual save location
 -
 - only thing worth noting is the use of `take 10` to ensure that we only
 - ever keep track of the 10 highest scores.
 -}
writeScore :: Int -> IO ()
writeScore newScore = do
    handle <- openFile "scoreFile.gal" ReadMode
    contents <- hGetContents handle 
    let scoreList = map read $ lines contents :: [Int]
    let newList = insertScore scoreList newScore    
    let newScores = unlines (map (show) (take 10 newList))
    (tempName, tempHandle) <- openTempFile "." "temp"
    hPutStr tempHandle $ newScores
    hClose handle
    hClose tempHandle
    removeFile "scoreFile.gal"
    renameFile tempName "scoreFile.gal"
    return ()
    
{- inserting the score
 -
 - simple recursive implementation because folding here still seems
 - counterintuitive to me.
 -}
insertScore :: [Int] -> Int -> [Int]
insertScore [] scr = [scr]
insertScore ss scr 
    | scr < head ss = head ss : insertScore (tail ss) scr
    | scr >= head ss = scr : ss

{- reading the score list
 -
 - pretty nifty little score reading function. the file only ever has one
 - number on each line, so we can just `read` an int straight off each line
 -}
readScoreList :: IO [Int]
readScoreList = do
    handle <- openFile "scoreFile.gal" ReadMode
    contents <- hGetContents handle 
    let scoreList = map read $ lines contents :: [Int]
    return scoreList
