{- debris module
 -
 - brief animation generated whenever a player ship dies.
 -
 - the choices made here are all very similar to those in the `Explosion` 
 - module. see that for any missing details. the only substantial 
 - differences between the two are the frames of animation and how they're
 - used by the world. 
 -
 - the justification for separating this and the enemy explosion into different
 - modules was the fact that the encompassing world doesn't use them in the 
 - same way. different events trigger them, and their animations are different
 -}
module Debris
( Debris(..)
, update
, mkDebris
, render
) where

import Graphics.Gloss
import Sprite

data DebrisState
    = Frame1
    | Frame2
    | Frame3
    deriving (Eq)

data Debris = Debris {
    state :: DebrisState,
    isAlive :: Bool,
    loc :: Point }

instance Sprite Debris where
    location = loc
    render = debRender
    size = debSize
    kill de = de { isAlive = False }

debSize :: Debris -> (Int, Int)
debSize _ = (35, 35)

debRender :: Debris -> IO Picture
debRender deb = 
    let path = case (state deb) of
                    Frame1 -> "assets/explosion/ship/frame1.bmp"
                    Frame2 -> "assets/explosion/ship/frame2.bmp"
                    Frame3 -> "assets/explosion/ship/frame3.bmp"
    in do
        expPic <- loadBMP path
        return $ translate (fst (loc deb)) (snd (loc deb)) expPic

update :: Debris -> Maybe Debris
update deb
    | Frame1 == s = Just (deb { state = Frame2 } )
    | Frame2 == s = Just (deb { state = Frame3 } )
    | Frame3 == s = Nothing
    where s = state deb

mkDebris :: Point -> Debris
mkDebris pt = Debris {
    state = Frame1,
    loc = pt,
    isAlive = True }
