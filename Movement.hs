{- moevement module
 -
 - contains some movement scripts for enemies
 -}
module Movement
( Movement(..)
, enemyMovement
, chargeMovement
, AccelVec(..)
, Time(..)
) where

import Graphics.Gloss

type AccelVec = (Float, Float)
type Time = Float

type Movement = [(AccelVec, Time)]

{- standard back-and-forth movement -}
enemyMovement :: Movement
enemyMovement =  
    [ ((-5.0, 0.0), 2.0)
    , (( 5.0, 0.0), 2.0)]
   {- [ ((0.0   ,(-5.0)), 2.0)
    , (((-5.0) ,  0.0), 2.0)
    , ((0.0   , 5.0), 2.0)
    , ((5.0  ,  0.0), 2.0)]
-}

{- the charging movement that happens every so often -}
chargeMovement :: Movement
chargeMovement =
    [ ((0.0, (-20.0)), 3)
    , ((0.0, ( 20.0)), 3) ]


