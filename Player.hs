{- player module
 -
 - keeps track of information related to the player including the number of 
 - lives left, the number of powerups left, the score, and the current level.
 -
 - the only powerup is a shield.
 -}

module Player
( Player(..)
, render
, kill
, mkPlayer
) where

import Graphics.Gloss
import GameConstants

data Player = Player {
    lives :: Int,
    level :: Int,
    score :: Int,
    shields :: Int}

{- rendering a player
 -
 - not much of note here. shields and lives are rendered in the lower-left
 - corner of the screen. the score is centered at the top.
 -}
render :: Player -> IO Picture
render player = do
    shipPic <- loadBMP "assets/models/ship_20.bmp"
    shieldP <- loadBMP "assets/models/shield.bmp"
    let (w, h) = GameConstants.screenSize
    let xbound = fromIntegral w / 2
    let ybound = fromIntegral h / 2
    let nShipPic = translate ((-xbound) + 20) ((-ybound) + 20) 
                    . scale 0.6 0.6 $ shipPic
    let count = translate ((-xbound) + 40) ((-ybound) + 15) 
                    . scale 0.1 0.1
                    . color white
                    . text $ "x" ++ (show $ lives player)
    let scoreText = translate (-30) (ybound - 20)
                    . scale 0.2 0.2
                    . color red
                    . text $ "score"
    let scorePic = translate (-20) (ybound - 60)
                    . scale 0.15 0.15
                    . color white
                    . text $ show (score player)
    let shieldCount = translate ((-xbound) + 90) ((-ybound) + 15) 
                        . scale 0.1 0.1
                        . color white
                        . text $ "x" ++ (show $ shields player)
    let shieldPic = translate (-xbound + 80) ((-ybound) + 20)
                        . scale 0.06 0.06 $ shieldP
    return $ Pictures $ nShipPic : count : scoreText : scorePic : 
                            shieldPic : shieldCount : []

{- killing a player
 -
 - has a different meaning than killing a sprite. the sprite implemetation
 - switched the `isAlive` bool. this one removes a life from the player's
 - reserve. removing the last will cause the game loop to push you to the 
 - 'Game Over' screen
 -}
kill :: Player -> Player
kill player = player { score = (score player - 1) }

{- making a player
 -
 - the player doesn't change throughout a `Game` game state session, so
 - you get 2 shields and 3 lives per playthrough. the score is eventually
 - passed to a handler that may write it to the score file if its high enough.
 - the level is incremented every time you kill all the enemies on the screen.
 -}
mkPlayer :: Player
mkPlayer = Player {
    lives = 3,
    level = 1,
    score = 0,
    shields = 2}
