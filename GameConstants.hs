{- gameconstants module
 -
 - these things don't change throughout a game. good place to define
 - things like the acceleration and drag that affects movement, and the speed
 - of bullets.
 -
 -}
module GameConstants 
(
    screenSize, 
    screenWidth,
    screenHeight,
    Size,
    fps,
    naturalAcceleration,
    naturalDrag,
    scaleFac,
    bulletSpeed
) where 

import Graphics.Gloss

type Size = (Int,Int)

scaleFac :: Float
scaleFac = 1

screenSize :: Size 
screenSize = (800,600)

screenWidth :: Int 
screenWidth = fst screenSize

screenHeight :: Int 
screenHeight = snd screenSize

{- this and natural drag are both used in calculations for the ship and enemy 
 - movement 
 -}
naturalAcceleration :: Float
naturalAcceleration = 40.0 

naturalDrag :: Float
naturalDrag = 0.9 

bulletSpeed :: Float
bulletSpeed = 400.0

fps :: Int
fps = 60 
